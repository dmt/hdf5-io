program io_hdf5
!module io_hdf5
  use readfile, only : read_file
  use h5md_module
  use hdf5
 
  use, intrinsic :: iso_fortran_env, only:  dp => real64

  implicit none

  !private
  integer :: herr
  integer, dimension(3) :: data_size
  integer, allocatable, dimension(:,:) :: part_idx
  integer, allocatable, dimension(:) :: steps
  real(kind=dp), allocatable, dimension(:) :: times
  real(kind=dp), allocatable,dimension(:,:,:) :: cellvec
  real(kind=dp), allocatable,dimension(:,:,:) :: pos
  real(kind=dp), allocatable,dimension(:,:,:) :: vel
  real(kind=dp), allocatable,dimension(:,:,:) :: force
  character(len=10), dimension(:,:), allocatable :: species !! char for now. should be enumerator/integer of atom type acc to h5mdspec.
    !public :: write_trajectory
  type data_info
     integer :: traj_key
     integer :: period_key
     real(kind=dp) :: delt
     integer ::  ndim=3 
     integer :: h5md_mode
     integer :: data_size(3)
     integer :: step
     integer :: time
     integer :: step_offset
     real(kind=dp) :: time_offset
     logical :: first_step = .true.
  end type data_info
  type(data_info) :: data_i

     !integer, dimension(data_size(2)) :: part_id
     !character(len=10), dimension(data_size(2)) :: species_id

  call h5open_f(herr) ! Initialize

  call read_file('Ar.traj',cellvec, pos, vel, force, data_i%traj_key, &
                   &data_i%period_key, part_idx, steps, times, species, data_i%delt)

  !todo, mode should be determined from the nature of data to be written
  data_i%data_size = shape(pos)
  write(*,*) data_i%data_size
  !data_i % h5md_mode = H5MD_FIXED  !! here whole data is present at once, so use fixed; in practice this will be time dependent
  !data_i % h5md_mode = ior(H5MD_LINEAR, H5MD_STORE_TIME)  !! checked in all three modes
  data_i % h5md_mode = ior(H5MD_TIME, H5MD_STORE_TIME)
  ! other values in data_i set now 

  if(iand(data_i%h5md_mode,H5MD_FIXED) == H5MD_FIXED) then
    call write_trajectory('trajectory_fixed.h5', data_i, cellvec, pos, vel, force, &
                        & part_idx, species, steps, times) 

  else if(iand(data_i%h5md_mode,H5MD_LINEAR) == H5MD_LINEAR) then
    if(steps(1) /= 0) then
      data_i % step_offset = steps(1)
      data_i % time_offset = times(1)
    end if
    data_i%step = steps(2) - steps(1) !only here, in real case its the store interval
    data_i%time = times(2) - times(1) !only here, in real, corresp time interval
    deallocate(steps) ! since whole steps were read at once here
    deallocate(times)
    call write_trajectory('trajectory_const_step.h5', data_i, cellvec, pos, vel,force, &
                          & part_idx, species) 

  else if(iand(data_i%h5md_mode,H5MD_TIME) == H5MD_TIME) then
    data_i % step = steps(1)
    data_i % time = times(1)
    !call write_trajectory('trajectory_time.h5', data_i, cellvec, pos, vel,force, part_idx, species) ! actual case, this call will do
    call write_trajectory('trajectory_time.h5', data_i, cellvec, pos, vel,force, &
                           & part_idx, species, steps, times) 
  end if
    
  call h5close_f(herr)  ! Finalize

  if(allocated(cellvec)) deallocate(cellvec)
  if(allocated(pos)) deallocate(pos)
  if(allocated(vel)) deallocate(vel)
  if(allocated(steps)) deallocate(steps)
  if(allocated(times)) deallocate(times)
  if(allocated(part_idx)) deallocate(part_idx)
  if(allocated(species)) deallocate(species)
  
  contains

  subroutine write_trajectory(filename, data_i, cell_vecs, pos, vel, force, part_idx, species, steps, times)

    character(len=*), intent(in)  :: filename
    type(data_info), intent(inout) :: data_i
    real(kind=dp), intent(in), dimension(data_i%data_size(1),data_i%data_size(2),data_i%data_size(3)) :: cell_vecs
    real(kind=dp), intent(in), dimension(data_i%data_size(1),data_i%data_size(2),data_i%data_size(3)) :: pos
    real(kind=dp), intent(in), dimension(data_i%data_size(1),data_i%data_size(2),data_i%data_size(3)) :: vel
    real(kind=dp), intent(in), dimension(data_i%data_size(1),data_i%data_size(2),data_i%data_size(3)) :: force
    integer, dimension(data_i%data_size(2), data_i%data_size(3)), intent(in), optional :: part_idx
    character(len=10), dimension(data_i%data_size(2), data_i%data_size(3)), intent(in), optional :: species
    integer, dimension(data_i%data_size(3)), intent(in), optional :: steps
    real(kind=dp), dimension(data_i%data_size(3)), intent(in), optional :: times

    !! in case of time dependent data, write at each time
     integer :: step
     real(kind=dp) :: time 
     integer :: step_offset 
     real(kind=dp) :: time_offset 
     integer i, j, N
     integer h5md_mode

    type(h5md_file_t) :: hfile
    type(h5md_element_t) :: elemt_e
    type(h5md_element_t) :: pos_e, vel_e, f_e, id_e, s_e, edg_e
    integer(HID_T) :: part_id, boxid
    integer(HID_T) :: p_id, v_id, f_id, id_id, s_id
    character(len=100) :: particle_gname
    integer :: error
    character(len=20) :: boundary

    particle_gname = 'Ar_group'
    N =  data_i%data_size(3) ! nsteps

    if(data_i%period_key==0) then
      boundary='none'
    elseif(data_i%period_key>0 .and. data_i%period_key <4 ) then 
      boundary='periodic'
    end if
    
    h5md_mode = data_i%h5md_mode
    call hfile%create(filename, 'dlpoly_io','0.0','user','dummyemail') 
    call h5gcreate_f(hfile%particles, trim(particle_gname), part_id, error)

    call h5gcreate_f(part_id, 'box', boxid, error)
    call h5md_write_attribute(boxid, 'dimension', data_i%ndim)
    call h5md_write_attribute(boxid, 'boundary', [trim(boundary), trim(boundary), trim(boundary) ])

    if( iand(data_i%h5md_mode , H5MD_FIXED) == H5MD_FIXED) then

    !! this is not really time indep data, so creating the groups explicitly so as to accommodate time and step
    !! for true time indep data of simulation, groups are not needed, datasets are given names directly
      call h5gcreate_f(part_id, 'position', p_id, error)
      call h5gcreate_f(part_id, 'velocity', v_id, error)
      call h5gcreate_f(part_id, 'force', f_id, error)
      call h5gcreate_f(part_id, 'id', id_id, error)
      call h5gcreate_f(part_id, 'species', s_id, error)

      ! same instance of 'element' type is reused. because fixed type close after writing
      call elemt_e%create_fixed(p_id, 'value', pos)
      call elemt_e%create_fixed(p_id, 'step', steps)
      call elemt_e%create_fixed(p_id, 'time', times)
      call elemt_e% create_fixed(boxid, 'edges', cellvec)

      if(data_i % traj_key > 0) then
      call elemt_e%create_fixed(v_id, 'velocity', vel)  !! these could be value, step, time form.
        if(data_i % traj_key > 1) then
        call elemt_e%create_fixed(f_id, 'force', force)   !! ..here steps are the same for all data, so ..
        end if
      end if
      call elemt_e%create_fixed(id_id, 'id', part_idx)
      call elemt_e%create_fixed(s_id, 'species', species)

      call elemt_e % close
      call h5gclose_f(boxid, error)
      call h5gclose_f(p_id, error)
      call h5gclose_f(v_id, error)
      call h5gclose_f(f_id, error)
      call h5gclose_f(s_id, error)
      call h5gclose_f(id_id, error)
      call h5gclose_f(part_id, error)
      

    else if( iand(data_i%h5md_mode,H5MD_LINEAR) == H5MD_LINEAR) then
      step = data_i % step
      time = data_i% time
      step_offset = data_i% step_offset
      time_offset = data_i % time_offset
      
      if(data_i % first_step .eqv. .true.) then

      call edg_e% create_time(boxid, 'edges', cellvec(:,:,1), data_i%h5md_mode, step, time, step_offset, time_offset)
      call pos_e%create_time(part_id, 'position', pos(:,:,1), data_i%h5md_mode, step, time, step_offset, time_offset)
      if(data_i % traj_key > 0) then
        call vel_e%create_time(part_id, 'velocity', vel(:,:,1), data_i%h5md_mode, step, time, step_offset, time_offset)
        if(data_i % traj_key > 1) then
          call f_e%create_time(part_id, 'forces', force(:,:,1), data_i%h5md_mode, step, time, step_offset, time_offset)
        end if
      end if
      call s_e%create_time(part_id, 'species', species(:,1), data_i%h5md_mode, step, time, step_offset, time_offset)
      call id_e%create_time(part_id, 'id', part_idx(:,1), data_i%h5md_mode, step, time, step_offset, time_offset)
      data_i % first_step = .false.
      end if 

      do i = 1,N
        call edg_e % append(cellvec(:,:,i))
        call pos_e % append(pos(:,:,i))
        if(data_i % traj_key > 0) then
          call vel_e % append(vel(:,:,i))
          if(data_i % traj_key > 1) then
            call f_e % append(force(:,:,i))
          end if
        end if
        call s_e % append(species(:,i))
        call id_e % append(part_idx(:,i))
      end do

      !edg_e%close() ! closing cause error, todo
      !pos_e%close()
      !vel_e%close()
      !f_e %close()
      !s_e %close()
      !id_e%close()

    elseif( iand(data_i%h5md_mode,H5MD_TIME) == H5MD_TIME) then
      step = data_i % step
      time = data_i% time
      
      if(data_i % first_step .eqv. .true.) then

      call edg_e% create_time(boxid, 'edges', cellvec(:,:,1), data_i%h5md_mode, step, time)
      call pos_e%create_time(part_id, 'position', pos(:,:,1), data_i%h5md_mode, step, time)
      if(data_i % traj_key > 0) then
        call vel_e%create_time(part_id, 'velocity', vel(:,:,1), data_i%h5md_mode, step, time)
        if(data_i % traj_key > 1) then
          call f_e%create_time(part_id, 'forces', force(:,:,1), data_i%h5md_mode, step, time)
        end if
      end if
      call s_e%create_time(part_id, 'species', species(:,1), data_i%h5md_mode, step, time)
      call id_e%create_time(part_id, 'id', part_idx(:,1), data_i%h5md_mode, step, time)
      data_i % first_step = .false.

      end if 

      do i = 1,N
        !step = step+1             !! In real case, actual step and time should be passed
        !time = step * data_i%delt
        step = steps(i)
        time = times(i)
        call edg_e % append(cellvec(:,:,i), step, time)
        call pos_e % append(pos(:,:,i), step, time)
        if(data_i % traj_key > 0) then
          call vel_e % append(vel(:,:,i), step, time)
          if(data_i % traj_key > 1) then
            call f_e % append(force(:,:,i), step, time)
          end if
       end if
       call s_e % append(species(:,i), step, time)
       call id_e % append(part_idx(:,i), step, time)
     end do

      !edg_e%close()  ! todo
      !pos_e%close()
      !vel_e%close()
      !f_e %close()
      !s_e %close()
      !id_e%close()

   end if 

  end subroutine  write_trajectory

!end module io_hdf5
end program io_hdf5
