module readfile

  use iso_fortran_env, only : dp => real64 
  implicit none

  private
  integer ::  ierr, i, j, m, n
  integer :: nsteps, nrecs, nparticles
  integer ::  tstep
  real(kind=dp) :: time
  real(kind=dp) :: atom_mass, charge, d_orig
  character(len=50) :: rformat
  character(len=200) :: a, label
  public :: read_file
 
contains

  subroutine read_file(filename, cellvec, pos, vel, force, traj_key, period_key, &
                       &part_idx, steps, times, species, delt)

    character(len=*), intent(in) :: filename
    real(kind=dp), allocatable, dimension(:,:,:), intent(inout) :: cellvec 
    real(kind=dp), allocatable, dimension(:,:,:), intent(inout) :: pos 
    real(kind=dp), allocatable, dimension(:,:,:), intent(inout) :: vel 
    real(kind=dp), allocatable, dimension(:,:,:), intent(inout) :: force
    integer, intent(out) :: traj_key, period_key
    integer, allocatable, dimension(:,:), intent(out) :: part_idx
    integer, allocatable, dimension(:), intent(out) :: steps
    real(kind=dp), allocatable, dimension(:), intent(out) :: times
    character(len=*), dimension(:,:), allocatable, intent(out) :: species
    real(kind=dp), intent(out) :: delt

    open(unit=11, file=trim(filename), status='old', action='read', iostat=ierr)
  
    read(11,'(a)') a ! read first comment line
    read(11,*)traj_key, period_key, nparticles, nsteps, nrecs
  
    allocate( cellvec(3,3,nsteps))
    allocate( pos(3,nparticles,nsteps))

    if(traj_key > 0) then
      allocate( vel(3,nparticles,nsteps))
    endif
    if(traj_key >1) then
      allocate( force(3,nparticles,nsteps))
    endif

    allocate( part_idx(nparticles, nsteps))
    allocate( steps(nsteps))
    allocate( times(nsteps))
    allocate( species(nparticles, nsteps))
  
    do i=1,nsteps
      read(11,*)a, steps(i), nsteps, traj_key, period_key, delt, times(i)

      do m=1,3
        read(11, *)cellvec(1,m,i), cellvec(2,m,i), cellvec(3,m,i)
      end do
      
      select case(traj_key)
      case(0)
        do j=1,nparticles
          read(11,*) species(j,i), part_idx(j,i), atom_mass, charge, d_orig
          read(11, *)pos(1,j,i), pos(2,j,i), pos(3,j,i)
        end do
      case(1)
        do j=1,nparticles
          read(11,*) species(j,i), part_idx(j,i), atom_mass, charge, d_orig
          read(11, *)pos(1,j,i), pos(2,j,i), pos(3,j,i)
          read(11, *)vel(1,j,i), vel(2,j,i), vel(3,j,i)
        end do

      case(2)
        do j=1,nparticles
          read(11,*) species(j,i), part_idx(j,i), atom_mass, charge, d_orig
          read(11, *)pos(1,j,i), pos(2,j,i), pos(3,j,i)
          read(11, *)vel(1,j,i), vel(2,j,i), vel(3,j,i)
          read(11, *)force(1,j,i), force(2,j,i), force(3,j,i)
        end do
      end select
    end do
  end subroutine 
end module 

